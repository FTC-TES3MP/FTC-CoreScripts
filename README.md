# CoreScripts

This is the custom repo for FTCs script testing on 0.7.0. <br />
Do not use CoreScripts unless you know what you're doing, not compatable with existing player data.<br />
<br />
You can find individual scripts and install info in the "Scripts" folder.
