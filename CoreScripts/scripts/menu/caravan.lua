Menus["caravan wilderness"] = {
    text = "Where would you like this caravan to take you?",
    buttons = {
        { caption = "Molag Mar (20 gp)",
            destinations = {
                menuHelper.destinations.setDefault("lack of gold"),
                menuHelper.destinations.setConditional(nil,
                {
                    menuHelper.conditions.requireItem("gold_001", 20)
                },
                {
                    menuHelper.effects.removeItem("gold_001", 20),
                    menuHelper.effects.runGlobalFunction("logicHandler", "TeleportPlayerToLocation",
                        {menuHelper.variables.currentPid(), "12, -8", 104784, -61697, 623, 0.087, -1.773})
                })
            }
        },
        { caption = "Mount Kand (34 gp)",
            destinations = {
                menuHelper.destinations.setDefault("lack of gold"),
                menuHelper.destinations.setConditional(nil,
                {
                    menuHelper.conditions.requireItem("gold_001", 34)
                },
                {
                    menuHelper.effects.removeItem("gold_001", 34),
                    menuHelper.effects.runGlobalFunction("logicHandler", "TeleportPlayerToLocation",
                        {menuHelper.variables.currentPid(), "0, 0", 0, 0, 0, 0, 0})
                })
            }
        },
        { caption = "Erabenimsun Camp (50 gp)",
            destinations = {
                menuHelper.destinations.setDefault("lack of gold"),
                menuHelper.destinations.setConditional(nil,
                {
                    menuHelper.conditions.requireItem("gold_001", 50)
                },
                {
                    menuHelper.effects.removeItem("gold_001", 50),
                    menuHelper.effects.runGlobalFunction("logicHandler", "TeleportPlayerToLocation",
                        {menuHelper.variables.currentPid(), "0, 0", 0, 0, 0, 0, 0})
                })
            }
        },
        { caption = "Exit", destinations = nil }
    }
}

Menus["caravan busy"] = {
    text = "The caravan is taking a break. You'll have to come back later.",
    buttons = {
        { caption = "Ok", destinations = nil }
    }
}

Menus["lack of gold"] = {
    text = "You lack the gold required.",
    buttons = {
        { caption = "Back", destinations = { menuHelper.destinations.setFromCustomVariable("previousCustomMenu") } },
        { caption = "Ok", destinations = nil }
    }
}
