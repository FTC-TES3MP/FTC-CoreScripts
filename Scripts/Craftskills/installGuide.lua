--How to install Craftskills

--In 'player/base.lua' at the bottom above 'return BasePlayer' copy
--START

function BasePlayer:initCustomSkills()
	if self.data.customSkills == nil then
		self.data.customSkills = {
			Leatherworking = 0,
			Tailoring = 0,
			Smithing = 0
		}
	end
	if self.data.customSkillsProgress == nil then
		self.data.customSkillsProgress = {
			Leatherworking = 0,
			Tailoring = 0,
			Smithing = 0
		}
	end
end

function BasePlayer:IncreaseTailoring(diffValue)

	if diffValue - self.data.customSkillsProgress["Tailoring"] < 1 then
		xpValue = 1
	else
		xpValue = diffValue - self.data.customSkillsProgress["Tailoring"]
	end
	if self.data.customSkillsProgress["Tailoring"] < 3 then
		self.data.customSkillsProgress["Tailoring"] = self.data.customSkillsProgress["Tailoring"] + xpValue
	else
		if self.data.customSkills["Tailoring"] == 10 then
			self.data.customSkills["Tailoring"] = 10
		else
			self.data.customSkills["Tailoring"] = self.data.customSkills["Tailoring"] + (self.data.customSkillsProgress["Tailoring"] / 3)
			self.data.customSkillsProgress["Tailoring"] = 0
		end
	end
end

function BasePlayer:IncreaseLeatherworking(diffValue)

	if diffValue - self.data.customSkillsProgress["Leatherworking"] < 1 then
			xpValue = 1
		else
			xpValue = diffValue - self.data.customSkillsProgress["Leatherworking"]
	end
    if self.data.customSkillsProgress["Leatherworking"] < 3 then
        self.data.customSkillsProgress["Leatherworking"] = self.data.customSkillsProgress["Leatherworking"] + xpValue
	else
		if self.data.customSkills["Leatherworking"] == 10 then
			self.data.customSkills["Leatherworking"] = 10
		else
			self.data.customSkills["Leatherworking"] = self.data.customSkills["Leatherworking"] + (self.data.customSkillsProgress["Leatherworking"] / 3)
			self.data.customSkillsProgress["Leatherworking"] = 0
		end
	end
end

function BasePlayer:IncreaseSmithing(diffValue)

	if diffValue - self.data.customSkillsProgress["Smithing"] < 1 then
			xpValue = 1
		else
			xpValue = diffValue - self.data.customSkillsProgress["Smithing"]
	end
    if self.data.customSkillsProgress["Smithing"] < 3 then
        self.data.customSkillsProgress["Smithing"] = self.data.customSkillsProgress["Smithing"] + xpValue
    else
		if self.data.customSkills["Smithing"] == 10 then
			self.data.customSkills["Smithing"] = 10
		else
			self.data.customSkills["Smithing"] = self.data.customSkills["Smithing"] + (self.data.customSkillsProgress["Smithing"] / 3)
			self.data.customSkillsProgress["Smithing"] = 0
		end
    end
end

--END

--then in the same file, above 'customVariables = {}' and below 'recordLinks = {}' add
--START

		customSkills = {
			Leatherworking = 0,
			Tailoring = 0,
			Smithing = 0
		},
		customSkillsProgress = {
			Leatherworking = 0,
			Tailoring = 0,
			Smithing = 0
		},
		
--END

--now in 'commandHandler.lua' find 'elseif cmd[2] == "name" or cmd[2] == "player" then' and below the next 'end' add
--START

    elseif cmd[1] == "craftskill" then
		local tailorval = Players[pid].data.customSkills["Tailoring"]
		local leatherval = Players[pid].data.customSkills["Leatherworking"]
		local smithval = Players[pid].data.customSkills["Smithing"]
        tes3mp.SendMessage(pid, color.Aqua .. "===================================================\n" .. "Your crafting skills are currently:\n" .. "------------------------------------------------" .. "\nTailoring: " .. tailorval .. "\n" .. "Leatherworking: " .. leatherval .. "\n" .. "Smithing: " .. smithval .. "\n" ..
		"===================================================\n")
		
--END

--now in 'menuHelper.lua' below the 'requireSkill' function add
--START

function menuHelper.conditions.requireCustomSkill(inputName, inputValue)
    local condition = {
        conditionType = "customSkill",
        customSkillName = inputName,
        customSkillValue = inputValue
    }

    return condition
end

--END

--now in 'defaultCrafting.lua' under the function 'Menus["default crafting origin"]' you can add the line 'menuHelper.conditions.requireCustomSkill("$skillName", $skillValue)' below any 'menuHelper.conditions.requireItem'
--to check for a certain skill level on each craft.

--now in 'Menus["default crafting pillow white"]' in the same lua file, you can add `menuHelper.effects.runPlayerFunction("Increase$skillName", { $skillGainMultiplier })`
--to increment any skill on craft
